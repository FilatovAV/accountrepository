﻿using AccountRepository.Universe;
using System;

namespace AccountRepository
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();
            var users = new Users(repository);

            var account = new Account(Oracle.GetName(Oracle.Random.Next(0,2)), Oracle.GetLastName(), DateTime.Now.AddYears(-18));
            users.AddAccount(account);

            var allUsers = repository.GetAll();

            foreach (var item in allUsers)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}

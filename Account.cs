﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace AccountRepository
{
    public class Account
    {
        public Account(string firstName, string lastName, DateTime birthDate)
        {
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
        }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public DateTime BirthDate { get; private set; }
        public override string ToString()
        {
            var s = JsonConvert.SerializeObject(this);
            return s;
        }
    }
}
